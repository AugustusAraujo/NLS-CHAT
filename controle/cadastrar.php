<?php 

if(isset($_FILES['arquivo'])){
    $arquivo = $_FILES['arquivo'];
    
    if($arquivo['error'])
        die("Falha ao enviar arquivo");
    if($arquivo['size'] > 10097152)
        die("Arquivo muito grande!");
    
    $nomeinput = $_POST['novo-nome'];
    $usuario = $_POST['usuario'];
    $pasta = "arquivos/";
    $nomeDoArquivo = $arquivo['name'];
    $novoNomeDoArquivo = $nomeinput;
    $extensao = strtolower(pathinfo($nomeDoArquivo,PATHINFO_EXTENSION));

    if($extensao != "jpg" && $extensao != 'png' && $extensao != 'jpeg' && $extensao != 'avif' && $extensao != 'gif' )
        die("Tipo de arquivo não aceito"); 
    
    $deu_certo = move_uploaded_file($arquivo['tmp_name'], $pasta . $novoNomeDoArquivo . "." . $extensao);
    if($deu_certo)
        echo "Arquivo enviado com sucesso! Clique ali para acessá-lo <a target='_blank' href='arquivos/$novoNomeDoArquivo.$extensao'>VER</a>";
    else
        echo "Falha ao enviar";
}

?>
<title>Cadastro</title>
<form action="">
    <label for="Nome">Nome</label><br>
    <input type="text" placeholder="Insira seu Nome" name="novo-nome"><br>
    <label for="Nome">Usuário</label><br>
    <input type="text" placeholder="Insira seu Nome de Usuário" name="usuario"><br>
    <label for="Nome">Senha</label><br>
    <input type="text" placeholder="Insira sua nova Senha" name="senha"><br> <br>
    <button type="submit">Cadastrar-se</button>
</form>

